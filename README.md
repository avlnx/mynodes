This file holds instructions on how to create a new masternode.

If you have the wallet open, turn it off.


# Step 1: Generate node addresses

First we need to generate the new node's addresses. At least two addresses will
need to be created, possibly three. If you already have a payout address run the
following command:

Usage: `./gen_node_addresses.sh NODE_NAME PAYOUT_ADDRESS`
Example: `./gen_node_addresses.sh lqx-mn-3 Lbc5YR36ZFWQFekfZMgxwYy2cuExZYygwP`

Otherwise, run without the PAYOUT_ADDRESS and a new one will be created:

Usage: `./gen_node_addresses.sh NODE_NAME`
Example: `./gen_node_addresses.sh my-awesome-node`

This command can optionally run the necessary transactions as well. Otherwise
you can run them yourself. Instructions will be printed with specifics. You
will need at least 15 confirmations before creating the node (Step 3).


# Step 2: Setup Linux server

Create a new Linux box, pull the wallet with the following wget command:

`wget https://lqxcoin.com/assets/downloads/linux-x86_64.tar.gz`

Untar the file: `tar -xvf linux-x86_64.tar.gz`  [confirm command]

Rsync the data folder ~/.lqxcore from a recent box.

Delete the wallet.dat file and update lqx.conf with proper data.

[TODO: Script the above and put in a repo along with the wallet.]

Run the wallet to sync up the blockchain.

Check we're done with the sync with the `./lqx-cli mnsync status` command.


# Step 3: Actually create the node

When the node is synced up we are ready to run the next command.

Usage: `./create_node.sh SERVER_IP:5784`
Example: `./create_node.sh 159.200.57.66:5784`

This script will create the node and register the transactions.


# Step 4: Update the masternodeblsprivkey in lqx.conf in the server

Open the mns_created.dat file and take note of the operator_private_key. We
need to update the lqx.conf file in the server with this info.


# Step 5 (optional): Claim the operator rewards

If the operator reward wasn't claimed automatically we need to claim it now.

You need to supply the pro_tx_hash, an ip:port and an operator_private_key as an 
argument to the update_operator.sh script:

Usage: `./update_operator.sh pro_tx_hash ip:port operator_priv_key`
Example: `./update_operator.sh \
b89c0a7d0ff821991ff02959d5582551a4d893c32bbb97f16701f94f21042eb8 \
159.203.113.129:5784 \
01feec58deaabffb41766fe0993b7e81a50429483ebd94d7b0c8769f229d2802`
  
# Step 0: TODO: setup cron
Setup the droplet to restart lqxd if rebooted or crashed.
  
Done. Enjoy your new node.