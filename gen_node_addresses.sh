#!/bin/bash

node_name=$1
payout_addr=$2

if [[ "$#" -lt 1 ]]
then
  echo "Usage: ./gen_node_addresses.sh node_name [payout_address]"
  echo "Example: ./gen_node_addresses.sh lqx-mn-3 Lbc5YR36ZFWQFekfZMgxwYy2cuExZYygwP"
  echo "Example without payout_address: ./gen_node_addresses.sh mynode3"
  exit 1
fi

./start_lqx_core.sh

log_new_address()
{
    printf "New address: %s %s\n" $1 " $2"
}

# generate new masternode and owner-key addresses.
label="$node_name"
node_addr=$(./lqx-cli getnewaddress $label)
log_new_address $node_addr $label

label="$node_name-owner-key"
owner_addr=$(./lqx-cli getnewaddress $label)
log_new_address $owner_addr $label

if [[ -z "$payout_addr" ]]
then
    label="$node_name-payout"
    payout_addr=$(./lqx-cli getnewaddress $label)
    log_new_address $payout_addr $label
fi

# print addresses
node_addr_file='node_addresses.dat'
message="$node_addr:$owner_addr:$payout_addr"
echo "$message" >$node_addr_file

read -p "Send coins automatically? ATTENTION! This will send 1000 coins! (Y/n) " send_coins
if [[ "$send_coins" == [yY] ]]
then
    unlocked=$(./unlock_wallet.sh)
    if (( ! $unlocked ))
    then
        printf "\nCould not unlock wallet. Exiting"
        exit 0
    fi
    # send 1000 coins to node address
    printf "\nSending 1000 LQX to %s" $node_addr
    tx_node=$(./send_coins.sh $node_addr 1000)
    printf "\nTransaction id: %s" $tx_node
    # send 0.1 coins to payout address
    printf "\nSending 0.1 LQX to %s" $payout_addr
    tx_node=$(./send_coins.sh $payout_addr 0.1)
    printf "\nTransaction id: %s" $tx_node
else
    printf "\nYou have chosen to fund the collateral by yourself."
    printf "\nPlease send 1000 LQX to %s" $node_addr
    printf "\nPlease send 0.1 LQX to %s" $payout_addr
    printf "\nYou can send coins with the command ./send_coins address number_of_coins"
fi

# Finished, print more instructions
printf "\n\nThis script is done."
echo "While you wait for 15 confirmations on the transaction above setup LQX core on the server"
echo "After confirmed and setting up the server you can continue the process by running the command:"
echo ""
echo "./create_node.sh SERVER_IP_ADDRESS:5784"
echo ""
echo "Example: ./create_node.sh 159.68.169.8:5784"
echo ""
echo "Ps. The file node_addresses.dat contains the last set of addresses created. This file is used in the command above. Don't mess with it."
echo "Goodbye."
