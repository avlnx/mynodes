#!/bin/bash

recipient_address=$1
number_of_coins=$2

if (( "$#"!=2 ))
then
  echo "Usage: ./send_coins.sh recipient_address number_of_coins"
  echo "Example: ./send_coins.sh Lbc5YR36ZFWQFekfZMgxwYy2cuExZYygwP 1000"
  exit 1
fi

unlocked=$(./unlock_wallet.sh)
if (( ! $unlocked ))
then
    echo "Could not unlock wallet. Try again later"
    exit 1
fi

./lqx-cli sendtoaddress $recipient_address $number_of_coins

return 0