#!/bin/bash

lqxd_pid=$(pgrep lqxd)
if [[ -z lqxd_pid ]]
then
    echo "Starting lqxd wallet. Please wait 10s"
    ./lqxd &
    sleep 10
else
    echo "LQX Core already running. Moving on"
fi
return 0