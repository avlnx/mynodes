#!/bin/bash

unlock_for=$1
if [[ -z $unlock_for ]]
then
    unlock_for=300
fi

wallet_info=$(./lqx-cli getwalletinfo)
unlocked_until=$(echo $wallet_info | jq .unlocked_until)
if (( $unlocked_until==0 ))
then
    read -s -p "Wallet locked. Please input password: " password
    ./lqx-cli walletpassphrase ${password} $unlock_for &> /dev/null
    if [[ $? -eq 0 ]]
    then
        printf "\nWallet unlocked for %ds\n" $unlock_for
        exit 0
    else
        printf "\nInvalid password\n"
        exit 1
    fi
else
    printf "\nUnlocked until %s\n" $unlocked_until
fi