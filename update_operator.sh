#!/bin/bash

if (( "$#"!= 3 ))
then
  echo "You need to supply the pro_tx_hash, an ip:port and an operator_private_key as an argument"
  echo "Usage: ./update_operator.sh pro_tx_hash ip:port operator_priv_key"
  echo "Example: ./update_operator.sh b89c0a7d0ff821991ff02959d5582551a4d893c32bbb97f16701f94f21042eb8 159.203.113.129:5784 01feec58deaabffb41766fe0993b7e81a50429483ebd94d7b0c8769f229d2802"
  exit 1
fi

operator_address=LfF7CuRCkWGD9pcEDZfygFnc5TiRbumK8a
pro_tx_hash=$1
ip=$2
operator_priv_key=$3
# protx update_service proTxHash ipAndPort operatorKey (operatorPayoutAddress feeSourceAddress)

# unlock wallet
./unlock_wallet.sh
if (( $?!=0 ))
then
    echo "Failed. Please run update_operator on your own."
    exit 1
fi

result=$(./lqx-cli protx update_service $pro_tx_hash $ip $operator_priv_key $operator_address)
echo "$result"

# lock wallet again
./lqx-cli walletlock